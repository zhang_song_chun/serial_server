import binascii
import re
import sys, time, random
import plotly.validators.layout.xaxis
import plotly.validators.layout.xaxis.title
import plotly.validators.layout.yaxis
import plotly.validators.layout.yaxis.title
import flask_compress
import numpy as np
import PyQt5.QtCore as QtCore
import pyqtgraph as pg
from PyQt5 import QtGui
from PyQt5.QtGui import QPixmap, QFont, QPen, QPainter, QPalette, QCursor, QIcon
from PyQt5.QtCore import pyqtSignal, QTimer, QThread, Qt, QMutex, QObject
from PyQt5.QtChart import QChartView, QChart, QSplineSeries, QValueAxis, QDateTimeAxis
from login import Ui_login_Form
from login_2 import Ui_login_2
from mainwindows import Ui_MainWindow
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
from PyQt5.QtWidgets import QApplication, QLabel, QWidget, QMessageBox, QMainWindow, QTableWidgetItem, QDialog
from plot_util_class import Spline, MyplotFigure
from thread_utils import My_plot_thread, My_serial_thread
import pandas as pd
import webbrowser
from datetime import datetime

import patch
from dash import Dash
import plotly.graph_objs as go
import dash_core_components as dcc
import dash_html_components as html

from pytz import utc
from pytz import timezone
from timer_table import Ui_Form
from utils import int_to_hex, addZero, add0x, intToHex, checkValue

user_dict = pd.read_excel('data/name_dict.xls')
USER_DICT = {}
for i, j in zip(user_dict['username'], user_dict['password']):
    USER_DICT[str(i)] = str(j)


class dash_thread(QThread):
    def __init__(self):
        super(dash_thread, self).__init__()
        self.receive_data = [[0,0]]
        self.host = '127.0.0.1'
        self.port = 8051
        self.layout = go.Layout(xaxis={'title': 'time'}, yaxis={'title': 'Force/N'})

    def run(self):
        self.x = np.array(self.receive_data)[:, 0]
        self.y = np.array(self.receive_data)[:, 1]
        self.data = [
            {'x': self.x, 'y': self.y, 'type': 'plot', 'name': 'MOTOR'},
        ]

        app = Dash()

        app.layout = html.Div(children=[
            # H1标签
            html.H1(children="Hello Dash", style={'textAlign': 'center'}),
            # Div标签
            html.Div(children="Dash: A web application framework for Python", style={'textAlign': 'center'}),

            dcc.Graph(
                id='motor-graph',
                figure={
                    'data': self.data,
                    'layout': self.layout,
                })
        ])
        app.run_server(debug = False, host = self.host, port = self.port)

# class data_show_block(QDialog, Ui_dialog):
#     def __init__(self):
#         super(data_show_block, self).__init__()
#         self.setupUi(self)
#
#         pg.setConfigOption('antialias', True)
#         pg.setConfigOption('leftButtonPan', False)
#         pg.setConfigOption('background', 'w')
#         pg.setConfigOption('foreground', 'k')
#
#         self.pw = pg.PlotWidget(self)
#         self.verticalLayout.addWidget(self.pw)
#
#         self.xlist = [0]
#         self.ylist = [0]
#         self.xlist = np.arange(0, 10000)
#         self.ylist = np.sin(self.xlist)
#         self.plot_data = self.pw.plot(self.xlist, self.ylist, pen={'color': 'r', 'width': 1.5})

class time_table(Ui_Form, QWidget):
    table_signal = pyqtSignal(list, list)
    def __init__(self):
        super(time_table, self).__init__()
        self.setupUi(self)
        self.row = 8
        self.col = 6
        self.start_list = []
        self.stop_list = []
        self.save_pushButton.clicked.connect(self.save_func)
        self.return_pushButton.clicked.connect(self.close)

        self.tabel_init()

    def tabel_init(self):
        self.tableWidget.setRowCount(self.row)
        self.tableWidget.setColumnCount(self.col)
        self.tableWidget.setHorizontalHeaderLabels(['开始时间/时', '开始时间/分', '开始时间/秒', \
                                                    '结束时间/时', '结束时间/分', '结束时间/秒'])
        self.tableWidget.setVerticalHeaderLabels(['第一段', '第二段', '第三段', '第四段', '第五段', '第六段','第七段','第八段'])
        for i in range(self.row):
            for j in range(self.col):
                input_table_items = "00"
                newItem = QTableWidgetItem(input_table_items)
                newItem.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                self.tableWidget.setItem(i, j, newItem)

    def save_func(self):
        self.start_list = []
        self.stop_list = []
        for i in range(self.row):
            temp = ""
            for j in range(int(self.col // 2)):
                temp += self.tableWidget.item(i, j).text() + ':'
            self.start_list.append(temp[:-1])

        for i in range(self.row):
            temp = ""
            for j in range(int(self.col // 2), self.col):
                temp += self.tableWidget.item(i, j).text() + ':'
            self.stop_list.append(temp[:-1])
        # print(self.start_list, self.stop_list)
        self.table_signal.emit(self.start_list, self.stop_list)


class Main_widget(QWidget, Ui_login_Form):
    def __init__(self):
        super(Main_widget, self).__init__()
        self.setupUi(self)

        # 用于与窗口2链接
        self.login_2_page = login_widget_2()
        self.main_window = Main_Window()
        # 设置实例
        self.CreateItems()
        # 设置信号与槽
        self.CreateSignalSlot()

    def CreateItems(self):
        pass

    def CreateSignalSlot(self):
        self.login_pushButton.clicked.connect(self.login_show_func)
        self.login_2_page.my_signal_1.connect(self.login_success_func)

    def login_show_func(self):
        self.login_2_page.show()
        # print(self.login_2_page.login_success_flag)
    def login_success_func(self):
        if self.login_2_page.login_success_flag:
            print('login success')
            self.close()
            self.login_2_page.close()
            # self.main_window.showMaximized()
            self.main_window.show()
        else: pass


class login_widget_2(QWidget, Ui_login_2):
    # 一定要放在init前面，不然会报错
    my_signal_1 = pyqtSignal(bool)
    def __init__(self):
        super(login_widget_2, self).__init__()
        self.setupUi(self)

        self.line_edit_init()
        self.createItems()
        self.createSignalSlot()

        self.login_success_flag = False

    def line_edit_init(self):
        self.login_pushButton2.setEnabled(False)
        self.username_line.setPlaceholderText('请输入用户名')
        self.password_line.setPlaceholderText('请输入密码')
        self.username_line.textChanged.connect(self.check_input_func)
        self.password_line.textChanged.connect(self.check_input_func)

    # 全部有输入才允许登录
    def check_input_func(self):
        if self.username_line.text() and self.password_line.text():
            self.login_pushButton2.setEnabled(True)
        else:
            self.login_pushButton2.setEnabled(False)

    # 创建实例
    def createItems(self):
        pass

    # 创建信号与槽
    def createSignalSlot(self):
        self.login_pushButton2.clicked.connect(self.check_login_func)
        self.reset_pushButton.clicked.connect(self.reset_func)

    # 判断登录是否成功
    def check_login_func(self):
        if self.password_line.text() == USER_DICT.get(self.username_line.text()):
            QMessageBox.information(self, 'success', 'login successfully')
            self.login_success_flag = True
            self.my_signal_1.emit(self.login_success_flag)
        else:
            QMessageBox.critical(self, 'warning', 'login failed')

        self.username_line.clear()
        self.password_line.clear()

    def reset_func(self):
        self.username_line.clear()
        self.password_line.clear()

# 主界面
class Main_Window(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(Main_Window, self).__init__()
        self.setupUi(self)
        self.setWindowOpacity(0.9)
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        pe = QPalette()
        self.setAutoFillBackground(True)
        pe.setColor(QPalette.Window, Qt.lightGray)  # 设置背景色
        self.setPalette(pe)
        self.pushbutton_close.setStyleSheet('''QPushButton{background:#F76677;border-radius:15px;}
        QPushButton:hover{background:red;}''')
        self.pushButton_2.setStyleSheet('''QPushButton{background:#F7D674;border-radius:15px;}
        QPushButton:hover{background:yellow;}''')
        self.pushbutton_mini.setStyleSheet('''QPushButton{background:#6DDF6D;border-radius:15px;}
        QPushButton:hover{background:green;}''')
        pg.setConfigOption('antialias', False)
        pg.setConfigOption('leftButtonPan', False)
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')


        self.time_table1 = time_table()
        self.time_table2 = time_table()
        self.settime_pushButton.clicked.connect(self.time_table1_func)
        self.settime_pushButton_2.clicked.connect(self.time_table2_func)
        self.time_table1.table_signal.connect(self.time_send_func1)
        self.time_table2.table_signal.connect(self.time_send_func2)

        self.hold_hand = False
        self.start_time_list1 = []
        self.start_time_list2 = []
        self.stop_time_list1 = []
        self.stop_time_list2 = []

        self.receive_data = []
        # global receive_data;
        # receive_data = []
        self.force_update_list = [0]
        self.location_current = 0
        self.current_force_idx = 0
        self.hexSending_checkBox.setChecked(True)
        self.hexShowing_checkBox.setChecked(True)

        self.jiaoyan_flag = False
        self.stop_flag = False
        self.start_query_flag = False
        self.force_time_update_flag = False
        self.right_move_flag = False
        self.left_move_flag = False
        self.start_pushButton_1_flag = False
        self.start_pushButton_2_flag = False
        self.yuandian_flag = False
        self.current_stop_flag = False
        self.Com_Baud_Combo.setCurrentText("115200")
        self.force_label.setText("force id:  F" + str(0))
        self.param = pd.read_excel('data/param.xlsx')
        self.weiyi_param = pd.read_excel('data/weiyi_param.xlsx')
        self.weiyi_head = self.weiyi_param.columns.values.tolist()

        self.save_data = pd.DataFrame(columns=['时间', '数据'])

        self.smooth_weight = 0.7
        self.smooth_last_num = self.force_update_list[-1]
        self.smooth_val = 0

        self.host = "127.0.0.1"
        self.port = "8051"
        self.host_lineEdit.setText("127.0.0.1")
        self.port_lineEdit.setText("8051")
        self.smooth_SpinBox.setValue(0.7)
        self.host_lineEdit.textChanged.connect(self.host_port_func)
        self.port_lineEdit.textChanged.connect(self.host_port_func)
        self.start_pushButton.setEnabled(False)
        self.stop_pushButton.setEnabled(False)

        self.stop_pushButton_2.setEnabled(False)
        self.leftmove_pushButton.setEnabled(False)
        self.rightmove_pushButton.setEnabled(False)
        self.start_pushButton_2.setEnabled(False)
        self.right_limit_line.textChanged.connect(self.right_lim_text_change_func)
        self.left_limit_line.textChanged.connect(self.right_lim_text_change_func)
        self.speed_set_line.textChanged.connect(self.right_lim_text_change_func)
        self.circle_time_line_2.textChanged.connect(self.right_lim_text_change_func)
        self.distance_line.textChanged.connect(self.distance_line_change_func)
        self.pushButton_2.clicked.connect(self.resetapp_func)
        self.stop_pushButton_2.clicked.connect(self.stop_query_func)
        self.no_force_location_line.textChanged.connect(self.stop_line_change_func)
        self.no_force_location_line_2.textChanged.connect(self.stop_line_change_func)

        self.param_table_init()
        self.plot_init_func()
        # 设置实例
        self.CreateItems()
        # 设置信号与槽
        self.CreateSignalSlot()

    def param_table_init(self):
        # 表格读取显示
        # self.tableWidget.currentIndex().data()
        self.save_param_button.clicked.connect(self.table_update_func)
        self.input_table_rows = self.param.shape[0]
        # 获取表格行数
        self.input_table_colunms = self.param.shape[1]
        self.input_table_header = self.param.columns.values.tolist()
        self.tableWidget.setColumnCount(self.input_table_colunms)
        self.tableWidget.setRowCount(self.input_table_rows)
        self.tableWidget.setHorizontalHeaderLabels(self.input_table_header)

        for i in range(self.input_table_rows):
            input_table_rows_values = self.param.iloc[[i]]
            input_table_rows_values_array = np.array(input_table_rows_values)
            input_table_rows_values_list = input_table_rows_values_array.tolist()[0]
            for j in range(self.input_table_colunms):
                input_table_items_list = input_table_rows_values_list[j]

                input_table_items = str(input_table_items_list)
                newItem = QTableWidgetItem(input_table_items)
                newItem.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                self.tableWidget.setItem(i, j, newItem)

    def table_update_func(self):
        self.data_frame = pd.DataFrame(self.param.values, columns=self.input_table_header)

        self.weiyi_data_frame = pd.DataFrame(np.array(list(map(eval,[self.right_limit_line.text(),
                                              self.left_limit_line.text(),
                                              self.speed_set_line.text(),
                                              self.circle_time_line_2.text(),
                                              self.distance_line.text(),
                                              self.no_force_location_line_2.text()]))).reshape(1,6),
                                             columns=self.weiyi_head)
        for i in range(self.input_table_rows):
            for j in range(self.input_table_colunms):
                self.data_frame.iloc[i, j] = int(self.tableWidget.item(i, j).text())

        self.param = self.data_frame
        print(self.data_frame)
        self.weiyi_data_frame.to_excel('data/weiyi_param.xlsx', index=False)
        self.data_frame.to_excel('data/param.xlsx', index=False)

    def plot_init_func(self):
        self.canvas = MyplotFigure()
        self.force_time_plot_layout.addWidget(self.canvas)

        # self.long_plot = data_show_block()

        self.pw1 = pg.PlotWidget(self, title = "force-time plot")
        self.pw2 = pg.PlotWidget(self, title = "force-time plot")

        self.pw1.setLabel('left', "force/N")
        self.pw1.setLabel('bottom', "time", units='s')
        self.pw1.showGrid(x=True, y=True)
        # self.pw1.setYRange(-40, 40, padding=0)
        self.pw2.setLabel('left', "force/N")
        self.pw2.setLabel('bottom', "time", units='s')
        self.pw2.showGrid(x=True, y=True)
        # self.pw2.setYRange(-40, 40, padding=0)
        # self.spline1 = Spline()
        # self.spline2 = Spline()
        # self.views1 = QChartView(self.spline1)
        # self.views2 = QChartView(self.spline2)
        #
        # # 设置为抗锯齿
        # self.views1.setRenderHint(QPainter.Antialiasing)
        # self.views1.setRubberBand(QChartView.RectangleRubberBand)
        # self.views2.setRenderHint(QPainter.Antialiasing)
        # self.views2.setRubberBand(QChartView.RectangleRubberBand)

        # self.drag_force_plot_layout.addWidget(self.views1)
        # self.drag_force_plot_layout_2.addWidget(self.views2)
        self.drag_force_plot_layout.addWidget(self.pw1)
        self.drag_force_plot_layout_2.addWidget(self.pw2)

        self.xlist = [0]
        self.ylist = [0]
        self.x = 0
        # self.xlist = np.arange(0, 300)
        # self.ylist = np.sin(self.xlist)

        self.plot_data1 = self.pw1.plot(self.xlist, self.ylist, pen={'color': 'r', 'width': 1.5}, title = 'force time plot')
        self.plot_data2 = self.pw2.plot(self.xlist, self.ylist, pen={'color': 'r', 'width': 1.5}, title = 'force time plot')

    def CreateItems(self):
        self.cst_tz = timezone('Asia/Shanghai')
        # Qt 串口类
        self.com = QSerialPort()
        # Qt 定时器类
        self.timer = QTimer(self)  # 初始化一个定时器
        self.timer.start(500)
        self.plot_thread = My_plot_thread()
        self.serial_thread = My_serial_thread()
        self.dash_thread = dash_thread()

    # 鼠标拖动
    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.m_flag = True
            self.m_Position = event.globalPos() - self.pos()  # 获取鼠标相对窗口的位置
            event.accept()
            self.setCursor(QCursor(QtCore.Qt.OpenHandCursor))  # 更改鼠标图标

    def mouseMoveEvent(self, QMouseEvent):
        if QtCore.Qt.LeftButton and self.m_flag:
            self.move(QMouseEvent.globalPos() - self.m_Position)  # 更改窗口位置
            QMouseEvent.accept()

    def mouseReleaseEvent(self, QMouseEvent):
        self.m_flag = False
        self.setCursor(QCursor(QtCore.Qt.ArrowCursor))

    # 鼠标左键双击
    def mouseDoubleClickEvent(self, a0: QtGui.QMouseEvent):
        if a0.button() == Qt.LeftButton and \
                (self.tabWidget.currentIndex() == 0 or self.tabWidget.currentIndex() == 1):
            choice = QMessageBox.question(self, '保存？', '你想对数据进行保存吗？',
                                 QMessageBox.Yes | QMessageBox.No)
            if choice == QMessageBox.Yes:
                self.save_data = pd.DataFrame(self.receive_data, columns=['time', '数据'])
                self.save_data.to_csv("data/save_data.csv", index=False)


    def CreateSignalSlot(self):
        # 按键切换界面
        self.drag_mode_button.clicked.connect(lambda : self.index_change_func(0))
        self.location_mode_button.clicked.connect(lambda : self.index_change_func(1))
        self.peifang_button.clicked.connect(lambda : self.index_change_func(2))
        self.data_button.clicked.connect(lambda : self.index_change_func(3))
        self.time_button.clicked.connect(lambda : self.index_change_func(4))
        self.pushbutton_close.clicked.connect(self.close_func)
        self.pushbutton_mini.clicked.connect(self.showMinimized)
        self.yuandian_pushButton.clicked.connect(self.yuandian_func)
        self.yuandian_pushButton_2.clicked.connect(self.yuandian_func)
        self.current_stop_pushButton.clicked.connect(self.current_stop_func)
        self.current_stop_pushButton_2.clicked.connect(self.current_stop_func)
        self.timer.timeout.connect(self.Timeout)  # 每隔0.1s显示时间

        self.jiaoyan_button.clicked.connect(self.jiaoyan_func)
        self.start_thread_Button.clicked.connect(self.start_thread_all_func)
        self.stop_thread_Button.clicked.connect(self.stop_thread_all_func)
        self.start_pushButton_2.clicked.connect(self.start_button_2_func)
        self.start_pushButton.clicked.connect(self.start_button_1_func)
        self.plot_pushButton.clicked.connect(self.plot_func1)
        # self.stop_pushButton.clicked.connect(self.stop_plot_func)
        self.stop_pushButton.clicked.connect(self.stop_query_func)
        self.rightmove_pushButton.clicked.connect(self.right_move_func)
        self.leftmove_pushButton.clicked.connect(self.left_move_func)
        self.use_param_button.clicked.connect(self.use_param_func)

        self.plot_thread._signal_updateUI.connect(self.plot_thread_slot)
        self.serial_thread._signal_serial.connect(self.handle_serial_func)

        self.Com_Open_Button.clicked.connect(self.Com_Open_Button_clicked)
        self.Com_Close_Button.clicked.connect(self.Com_Close_Button_clicked)
        self.Send_Button.clicked.connect(self.SendButton_clicked)
        self.Com_Refresh_Button.clicked.connect(self.Com_Refresh_Button_Clicked)
        self.com.readyRead.connect(self.Com_Receive_Data)  # 接收数据
        self.com.readyRead.connect(self.Com_assistant_Receive_Data)  # 接收数据
        self.hexSending_checkBox.stateChanged.connect(self.hexShowingClicked)
        self.hexSending_checkBox.stateChanged.connect(self.hexSendingClicked)
        self.ClearButton.clicked.connect(self.clear_text)

        self.plot_long_pushButton.clicked.connect(self.show_dash_plot)

    def host_port_func(self):
        self.host = self.host_lineEdit.text()
        self.port = self.port_lineEdit.text()

        self.dash_thread.host = self.host
        self.dash_thread.port = int(self.port)

    '''
    dash绘图
    '''
    def show_dash_plot(self):
        self.dash_thread.terminate()
        self.dash_thread.start()
        webbrowser.open("http://" + self.host + ":" + self.port, 0, False)

        # 点击一次后切换端口,并更新数字
        self.dash_thread.port += 1
        self.port = str(int(self.port) + 1)
        self.port_lineEdit.setText(self.port)
    '''
    设置定时功能
    '''
    def time_table1_func(self):
        self.time_table1.show()

    def time_table2_func(self):
        self.time_table2.show()

    def time_send_func1(self, start_list, stop_list):
        self.start_time_list1 = start_list.copy()
        self.stop_time_list1 = stop_list.copy()
        # print(self.start_time_list1, self.stop_time_list1)

    def time_send_func2(self, start_list, stop_list):
        self.start_time_list2 = start_list.copy()
        self.stop_time_list2 = stop_list.copy()
        # print(self.start_time_list2, self.stop_time_list2)

    def use_param_func(self):
        index = self.param_set_spin.value() - 1
        data_use = self.param.iloc[index, :]
        data_weiyi = self.weiyi_param.values[0]
        print(data_weiyi)

        self.right_limit_line.setText(str(data_weiyi[0]))
        self.left_limit_line.setText(str(data_weiyi[1]))
        self.speed_set_line.setText(str(data_weiyi[2]))
        self.circle_time_line_2.setText(str(data_weiyi[3]))
        self.distance_line.setText(str(data_weiyi[4]))
        self.no_force_location_line_2.setText(str(data_weiyi[5]))

        self.t0_line.setText(str(data_use[0]))
        self.f0_line.setText(str(data_use[1]))
        self.t1_line.setText(str(data_use[2]))
        self.f1_line.setText(str(data_use[3]))
        self.t2_line.setText(str(data_use[4]))
        self.f2_line.setText(str(data_use[5]))
        self.t3_line.setText(str(data_use[6]))
        self.f3_line.setText(str(data_use[7]))
        self.t4_line.setText(str(data_use[8]))
        self.f4_line.setText(str(data_use[9]))
        self.t5_line.setText(str(data_use[10]))
        self.f5_line.setText(str(data_use[11]))
        self.t6_line.setText(str(data_use[12]))
        self.f6_line.setText(str(data_use[13]))
        self.circle_location2_line.setText(str(data_use[15]))
        self.circle_location1_line.setText(str(data_use[14]))
        self.circle_times_line.setText(str(data_use[16]))

    def resetapp_func(self):
        QMessageBox.information(self, 'reset', '立即进行重启？')
        time.sleep(2)
        QApplication.exit(888)

    def right_lim_text_change_func(self):
        if self.right_limit_line.text() and self.left_limit_line.text() and \
            self.speed_set_line.text() and self.circle_time_line_2.text() and \
                self.no_force_location_line_2.text() and \
                eval(self.speed_set_line.text()) in list(map(lambda x : x / 10, [1,2,3,4,5,6,8,9,10,12,15,16,18,20,24,25,30,40])):
            self.start_pushButton_2.setEnabled(True)
        else:
            self.start_pushButton_2.setEnabled(False)

        if self.speed_set_line.text() and \
                eval(self.speed_set_line.text()) not in list(map(lambda x : x / 10, [1,2,3,4,5,6,8,9,10,12,15,16,18,20,24,25,30,40])):
            QMessageBox.information(self, 'waring', '速度必须在' + str(list(map(lambda x : x / 10, [1,2,3,4,5,6,8,9,10,12,15,16,18,20,24,25,30,40]))) + '中选择')
        else:pass

    def stop_line_change_func(self):
        if self.no_force_location_line.text():
            self.stop_pushButton.setEnabled(True)
            self.start_pushButton.setEnabled(True)
        elif self.no_force_location_line.text() is None:
            self.start_pushButton.setEnabled(False)

        if self.no_force_location_line_2.text():
            self.stop_pushButton_2.setEnabled(True)
            self.start_pushButton_2.setEnabled(True)
        else:
            self.stop_pushButton_2.setEnabled(False)
            self.start_pushButton_2.setEnabled(False)

    def distance_line_change_func(self):
        if self.distance_line.text():
            self.leftmove_pushButton.setEnabled(True)
            self.rightmove_pushButton.setEnabled(True)

    def index_change_func(self, num):
        self.tabWidget.setCurrentIndex(num)

    def jiaoyan_func(self):
        if self.jiaoyan_flag == True:
            self.jiaoyan_flag = False
            self.jiaoyan_button.setText("开启校验")
        else:
            self.jiaoyan_flag = True
            self.jiaoyan_button.setText("关闭校验")


    def Timeout(self):
        self.time_now = time.strftime("%H:%M:%S", time.localtime())
        self.Time_label.setText(time.strftime("%B %d, %H:%M:%S", time.localtime()))
        if self.time_now in self.start_time_list1 and self.time_now != "00:00:00" :
            self.force_time_update_flag = True
        if self.time_now in self.stop_time_list1 and self.time_now != "00:00:00" :
            self.stop_flag = True
        if self.time_now in self.start_time_list2 and self.time_now != "00:00:00" :
            self.start_pushButton_2_flag = True
        if self.time_now in self.stop_time_list2 and self.time_now != "00:00:00" :
            self.stop_flag = True

    # 画静态图
    '''
    绘制两幅图像
    '''
    def plot_func1(self):
        # 数据处理
        times_new = []
        force_new = []

        circle_idx = [self.circle_location1_line.text(), self.circle_location2_line.text()]
        circle_location = list(map(int, circle_idx))
        circle_num = int(self.circle_times_line.text())
        self.time_str = [self.t0_line.text(), self.t1_line.text(), self.t2_line.text(), \
                     self.t3_line.text(), self.t4_line.text(), self.t5_line.text(), self.t6_line.text()]
        self.force_str = [self.f0_line.text(), self.f1_line.text(), self.f2_line.text(), self.f3_line.text(),\
                      self.f4_line.text(), self.f5_line.text(), self.f6_line.text()]

        self.time = list(map(eval,self.time_str))
        self.force = list(map(eval,self.force_str))

        # 第一位是0
        f_new_list = self.force[0: circle_location[0]] + \
                     self.force[circle_location[0]: int(circle_location[1] + 1)] * circle_num + \
                     self.force[circle_location[1] + 1:]

        new_time_list = self.time[0: circle_location[0]] + \
                        self.time[circle_location[0]: int(circle_location[1] + 1)] * circle_num + \
                        self.time[circle_location[1] + 1:]

        self.time = list(map(lambda x: int(10 * x), self.time))
        self.force = list(map(lambda x: int(100 * x), self.force))
        # print(new_time_list)
        # print(f_new_list)
        new_time_list = np.cumsum(new_time_list)
        new_time_list = np.insert(new_time_list, 0, 0)
        #
        time_x = np.linspace(0, new_time_list[-1], 1000)
        force_y = np.zeros_like(time_x)
        for i in range(len(time_x)):
            for j in range(len(new_time_list) - 1):
                if new_time_list[j] <= time_x[i] < new_time_list[j + 1]:
                    force_y[i] = f_new_list[j]

        self.canvas.figs.set_label('time')
        self.canvas.figs.suptitle('force-time')
        # self.canvas.figs.set_ylabel(fontsize=17)
        self.canvas.matplot_drow_axes(time_x, force_y)

    def plot_func(self):
        # time_list = [1,2,3,4]
        # force_list = [7,8,9,100]

        # self.x = np.array(time_list)
        # self.y = np.array(force_list)

        self.canvas.matplot_drow_axes(self.x, self.y)
        self.canvas.figs.suptitle('sin')

    def start_thread_all_func(self):
        self.plot_thread.start()
        self.serial_thread.start()
        self.start_query_flag = True
        # self.force_time_update_flag = True

    def stop_thread_all_func(self):
        self.plot_thread.stop()
        self.plot_thread.isexit = False
        # self.serial_thread.stop()

    # # 画左边实时动态图
    # def start_plot_func(self):
    #     self.plot_thread.start()
    #
    # def stop_plot_func(self):
    #     self.plot_thread.stop()
    #     self.plot_thread.isexit = False

    # 更新动态图数据
    def plot_thread_slot(self):
        self.x += 0.1
        if len(self.ylist) > 200:
            self.ylist.pop(0)
            self.xlist.pop(0)

        self.xlist.append(self.x)
        # 对曲线进行低通滤波

        self.smooth_weight = self.smooth_SpinBox.value()
        # print(self.smooth_Slider.value())
        self.smooth_val = self.smooth_last_num * self.smooth_weight + \
                          (1 - self.smooth_weight) * self.force_update_list[-1]
        # self.ylist.append(self.force_update_list[-1])
        if abs(self.smooth_val - self.smooth_last_num) > 4:
            self.ylist.append(self.smooth_last_num)
        else:
            self.ylist.append(self.smooth_val)
        self.smooth_last_num = self.smooth_val

        self.plot_data1.setData(self.xlist, self.ylist, pen={'color': 'r', 'width': 1.5}, title = 'force time plot')
        self.plot_data2.setData(self.xlist, self.ylist, pen={'color': 'r', 'width': 1.5}, title = 'force time plot')

        # 实时刷新
        QApplication.processEvents()

    '''
    串口助手
    以及串口通信处理
    '''
    def start_button_1_func(self):
        self.plot_pushButton.click()
        self.force_time_update_flag = True

    def start_button_2_func(self):
        self.start_pushButton_2_flag = True

    def right_move_func(self):
        self.right_move_flag = True

    def left_move_func(self):
        self.left_move_flag = True

    def yuandian_func(self):
        self.yuandian_flag = True

    def current_stop_func(self):
        self.current_stop_flag = True

    def stop_query_func(self):
        self.stop_flag = True
        # self.serial_thread.stop()

    def handle_serial_func(self):
        # 与下位机进行握手
        if self.hold_hand == False:
            hold_hand_code = '00'
            try:
                self.com.write(binascii.a2b_hex(hold_hand_code))
            except:
                pass

        else:
            if self.start_query_flag:
                query_code = '0800'
                query_code += checkValue(query_code)
                # self.textEdit_Send.setText(query_code)
                # self.Send_Button.click()
                try:
                    self.com.write(binascii.a2b_hex(query_code))
                except:
                    QMessageBox.warning(self, 'warning', 'serial problem')


                if self.force_time_update_flag:
                    # self.force_time_update_flag = False
                    send_str = '04'
                    # 计算非零元素个数 2 循环次数 1循环起始 1循环终止 2无力点位置
                    # send_str += int_to_hex(4 * (len(self.force) - self.force.count(0)) + 2 + 1 + 1 + 2, 1)
                    send_str += int_to_hex(4 * (len(self.force)) + 2 + 1 + 1 + 2, 1)
                    for fc, tm in zip(self.force, self.time):
                        # if fc != 0 and tm != 0:
                        send_str += (int_to_hex(fc, 2) + int_to_hex(tm, 2))
                    send_str += int_to_hex(int(self.circle_times_line.text()), 2)
                    # 将1-7变成0-6
                    send_str += (int_to_hex(int(self.circle_location1_line.text()) - 1, 1) + \
                                 int_to_hex(int(self.circle_location2_line.text()) - 1, 1))
                    send_str += int_to_hex(int(self.no_force_location_line.text()) * 100, 2)
                    # 计算校验位置
                    send_str += checkValue(send_str)
                    self.textEdit_Send.setText(send_str)
                    self.Send_Button.click()

                    self.force_time_update_flag = False

                elif self.left_move_flag:
                    # 03 00/ff 2b距离
                    send_str = '020300' + int_to_hex(int(eval(self.distance_line.text()) * 100), 2)
                    send_str += checkValue(send_str)
                    self.textEdit_Send.setText(send_str)
                    self.Send_Button.click()
                    self.left_move_flag = False

                elif self.right_move_flag:
                    send_str = '0203ff' + int_to_hex(int(eval(self.distance_line.text()) * 100), 2)
                    send_str += checkValue(send_str)
                    self.textEdit_Send.setText(send_str)
                    self.Send_Button.click()
                    self.right_move_flag = False

                elif self.stop_flag:
                    if self.tabWidget.currentIndex() == 0:
                        send_str = '0502' + int_to_hex(int(self.no_force_location_line.text()) * 100, 2)
                    elif self.tabWidget.currentIndex() == 1:
                        send_str = '0502' + int_to_hex(int(self.no_force_location_line_2.text()) * 100, 2)
                    send_str += checkValue(send_str)
                    self.textEdit_Send.setText(send_str)
                    self.Send_Button.click()
                    self.stop_flag = False

                #     self.serial_thread.stop()

                elif self.current_stop_flag:
                    send_str = '0600'
                    send_str += checkValue(send_str)
                    self.textEdit_Send.setText(send_str)
                    self.Send_Button.click()
                    self.current_stop_flag = False


                elif self.yuandian_flag:
                    send_str = '0103000000'
                    send_str += checkValue(send_str)
                    self.textEdit_Send.setText(send_str)
                    self.Send_Button.click()
                    self.yuandian_flag = False

                elif self.start_pushButton_2_flag:
                    send_str = '030A' + int_to_hex(int(100 * eval(self.left_limit_line.text())),2) + \
                                               int_to_hex(int(100 * eval(self.right_limit_line.text())),2) + \
                                               int_to_hex(int(10 * eval(self.speed_set_line.text())), 2) + \
                                               int_to_hex(int(self.circle_time_line_2.text()), 2)
                    send_str += int_to_hex(int(self.no_force_location_line_2.text()) * 100, 2)
                    send_str += checkValue(send_str)
                    self.textEdit_Send.setText(send_str)
                    self.Send_Button.click()
                    self.start_pushButton_2_flag = False

                self.textEdit_Send.clear()

    # 串口发送数据
    def Com_Send_Data(self):
        txData = self.textEdit_Send.toPlainText()
        if len(txData) == 0:
            return
        if self.hexSending_checkBox.isChecked() == False:
            self.com.write(txData.encode('UTF-8'))
        else:
            Data = txData.replace(' ', '')
            # 如果16进制不是偶数个字符, 去掉最后一个, [ ]左闭右开
            if len(Data) % 2 == 1:
                Data = Data[0:len(Data) - 1]
            # 如果遇到非16进制字符
            if Data.isalnum() is False:
                QMessageBox.critical(self, '错误', '包含非十六进制数')
            try:
                hexData = binascii.a2b_hex(Data)
            except:
                QMessageBox.critical(self, '错误', '转换编码错误')
                return
            # 发送16进制数据, 发送格式如 ‘31 32 33 41 42 43’, 代表'123ABC'
            try:
                self.com.write(hexData)
            except:
                QMessageBox.critical(self, '异常', '十六进制发送错误')
                return

        # 串口接收数据
    def Com_assistant_Receive_Data(self):
        pass
        # if self.hexShowing_checkBox.isChecked() == False:
        #     try:
        #         self.textEdit_Recive.insertPlainText(self.rxData.decode('UTF-8'))
        #     except:
        #         pass
        # else:
        #     Data = binascii.b2a_hex(self.rxData).decode('ascii')
        #     # re 正则表达式 (.{2}) 匹配两个字母
        #     hexStr = ' 0x'.join(re.findall('(.{2})', Data))
        #     # 补齐第一个 0x
        #     hexStr = '0x' + hexStr
        #     # 被勾选才显示数据
        #     if self.hexShowing_checkBox.isChecked() == True:
        #         self.textEdit_Recive.insertPlainText(hexStr)
        #         self.textEdit_Recive.insertPlainText(' ')

    def Com_Receive_Data(self):
        self.temp_arr = []
        try:
            self.rxData = bytes(self.com.readAll())
        except:
            QMessageBox.critical(self, '严重错误', '串口接收数据错误')

        # 对数据进行解包
        if self.rxData[0] == int(0x18):
            #没有校验
            if self.jiaoyan_flag == False:
                if len(self.rxData) == 2 * self.rxData[1] + 2 + 3:
                    # 此处需要解决十六进制转换的问题
                    self.current_force_idx = self.rxData[-3] >> 4
                    self.force_label.setText("force id:" + str(self.current_force_idx))
                    self.location_current = self.rxData[-1] << 8 | self.rxData[-2]
                    self.force_update_list = [self.rxData[2 + 2 * i + 1] << 8 | self.rxData[2 + 2 * i] for i in range(self.rxData[1])]
                    #数据换算
                    self.force_update_list = [int(50157 - 25.256 * num) / 100 for num in self.force_update_list]

                    now = datetime.now().replace(tzinfo=self.cst_tz)
                    self.time_str = now.strftime("%Y-%m-%d %H:%M:%S.%f")[:-5] # 毫秒级时间戳
                    self.time_str_list = []
                    #产生随机数排序
                    # rand_gen = list(map(str, np.sort(np.random.randint(0, 100 ,len(self.force_update_list)))))
                    rand_gen = list(map(str, map(int, list(np.arange(1, 100, 100 / len(self.force_update_list))))))
                    for i in range(len(self.force_update_list)):
                        self.receive_data.append([self.time_str + rand_gen[i], self.force_update_list[i]])

                    self.dash_thread.receive_data = self.receive_data.copy()
                    self.realtime_force_line.setText(str(self.force_update_list[-1]))
                    self.real_time_location_line.setText(str(self.location_current / 100))
                    self.realtime_force_line_2.setText(str(self.force_update_list[-1]))
                    self.real_time_location_line_2.setText(str(self.location_current / 100))
                # else: pass
            #开启了校验
            else:
                if len(self.rxData) == 2 * self.rxData[1] + 2 + 3 + 1:
                    str_check = ""
                    for i in range(len(self.rxData) - 1): str_check += int_to_hex(self.rxData[i], 1)
                    if int_to_hex(self.rxData[-1], 1) == checkValue(str_check):
                        self.location_current = self.rxData[-2] << 8 | self.rxData[-3]
                        self.force_update_list = [self.rxData[2 + 2 * i + 1] << 8 | self.rxData[2 + 2 * i] for i in
                                                  range(self.rxData[1])]
                        # 数据换算
                        self.force_update_list = [int(53776 - 25.342 * num) for num in self.force_update_list]

                        now = datetime.now().replace(tzinfo=self.cst_tz)
                        self.time_str = now.strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]  # 毫秒级时间戳
                        # self.time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                        self.time_str_list = []
                        # 产生随机数排序
                        rand_gen = list(map(str, np.sort(np.random.randint(0, 100, len(self.force_update_list)))))
                        for i in range(len(self.force_update_list)):
                            self.receive_data.append([self.time_str + rand_gen[i], self.force_update_list[i]])

                        self.dash_thread.receive_data = self.receive_data.copy()
                        self.realtime_force_line.setText(str(self.force_update_list[-1]))
                        self.real_time_location_line.setText(str(self.location_current / 100))
                        self.realtime_force_line_2.setText(str(self.force_update_list[-1]))
                        self.real_time_location_line_2.setText(str(self.location_current / 100))


        elif self.rxData[0] == int(0x00):
            self.hold_hand = True

    # 串口刷新
    def Com_Refresh_Button_Clicked(self):
        self.Com_Name_Combo.clear()
        com = QSerialPort()
        com_list = QSerialPortInfo.availablePorts()
        for info in com_list:
            com.setPort(info)
            if com.open(QSerialPort.ReadWrite):
                self.Com_Name_Combo.addItem(info.portName())
                com.close()

    # 16进制显示按下
    def hexShowingClicked(self):
        if self.hexShowing_checkBox.isChecked() == True:
            # 接收区换行
            self.textEdit_Recive.insertPlainText('\n')

    # 16进制发送按下
    def hexSendingClicked(self):
        if self.hexSending_checkBox.isChecked() == True:
            pass

    # 发送按钮按下
    def SendButton_clicked(self):
        self.Com_Send_Data()

    # 串口打开按钮按下
    def Com_Open_Button_clicked(self):
        #### com Open Code here ####
        comName = self.Com_Name_Combo.currentText()

        comBaud = int(self.Com_Baud_Combo.currentText())
        self.com.setPortName(comName)
        try:
            if self.com.open(QSerialPort.ReadWrite) == False:
                QMessageBox.critical(self, '严重错误', '串口打开失败')
                return
        except:
            QMessageBox.critical(self, '严重错误', '串口打开失败')
            return
        self.Com_Close_Button.setEnabled(True)
        self.Com_Open_Button.setEnabled(False)
        self.Com_Refresh_Button.setEnabled(False)
        self.Com_Name_Combo.setEnabled(False)
        self.Com_Baud_Combo.setEnabled(False)
        self.Com_isOpenOrNot_Label.setText('  已打开')
        self.com.setBaudRate(comBaud)

    def Com_Close_Button_clicked(self):
        self.com.close()
        self.Com_Close_Button.setEnabled(False)
        self.Com_Open_Button.setEnabled(True)
        self.Com_Refresh_Button.setEnabled(True)
        self.Com_Name_Combo.setEnabled(True)
        self.Com_Baud_Combo.setEnabled(True)
        self.Com_isOpenOrNot_Label.setText('  已关闭')

    def close_func(self):
        choice = QMessageBox.question(self, 'information','是否保存所有参数?',
                             QMessageBox.Yes|QMessageBox.No)
        if choice == QMessageBox.Yes:
            self.save_param_button.click()
            time.sleep(1)
            self.close()
        else:
            self.close()

    def clear_text(self):
        self.textEdit_Recive.clear()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    login = Main_widget()
    # login = time_table()
    login.show()
    sys.exit(app.exec_())

