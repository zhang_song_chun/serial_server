# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['main_ui.py'],
             pathex=['C:\\Users\\陈远强\\Desktop\\上位机\\server'],
             binaries=[],
             datas=[('data/name_dict.xls', '.'), ('data/param.xls', '.'), ('data/save_data.csv', '.'),
             ('pack_info/pack_core_info/package-info.json', './dash_core_components'), ('pack_info/pack_html_info/package-info.json', './dash_html_components'),
             ('js_file/core_js/*', './dash_core_components'),('js_file/html_js/*', './dash_html_components'),
             ('js_file/render_js*', './dash_renderer')
             ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='上位机',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          icon = 'pic/arm.ico')
