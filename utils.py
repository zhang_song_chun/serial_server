import binascii
import numpy as np

# 十进制转为为两位十六进制
def int_to_hex(num, bit):
    str = hex(num)[2:]
    str_ = '0' * (4 - len(str)) + str
    if bit == 2:
        return str_
    elif bit == 1:
        return str_[-2:]
def addZero(x, y):
    while True:
        if len(x) < y:
            x = '0' + x
        else:
            break

    return x
def intToHex(n, x):
     num = hex(n)
     #print(num)
     num_list = num.split('0x')[1:]#num_list = num.split('0x')[1]

     return addZero(num_list[0].upper(), x*2)

def add0x(s):
     return eval('0x'+s)

def checkValue(h):
     #先取前2组，每组2个做异或运算
     value = add0x(h[0:2]) ^ add0x(h[2:4])#异或后是10进制数

     for i in range(4, len(h), 2):
         value = value ^ add0x(h[i:i+2])
         #print('异或结果--10进制：',value)
         #print('异或结果--16进制：',hex(value))
         #print('转换成16进制intToHex(value, 1):',intToHex(value, 1))
         #value = add0x(intToHex(value, 1))

     value = intToHex(value, 1)#16进制的校验值，1个字节
     return value.upper()

if __name__ == '__main__':
    print(int_to_hex(int(100 * eval('10.1')), 2))
