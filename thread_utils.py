import time
from PyQt5.QtCore import QThread, pyqtSignal, QMutex, QObject
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
from PyQt5.QtWidgets import QMessageBox

class My_plot_thread(QThread):
    _signal_updateUI = pyqtSignal()
    def __init__(self):
        super(My_plot_thread, self).__init__()
        self.qmut = QMutex()
        self.isexit = False

    def run(self):
        while (True):
            self.qmut.lock()
            if (self.isexit):
                break
            self.qmut.unlock()
            self._signal_updateUI.emit()
            # 绘图频率
            time.sleep(0.1)
        self.qmut.unlock()

    def stop(self):
        # 改变线程状态与终止
        self.qmut.lock()
        self.isexit = True
        self.qmut.unlock()
        self.wait()

class My_serial_thread(QThread):
    _signal_serial = pyqtSignal()

    def __init__(self):
        super(My_serial_thread, self).__init__()
        self.qmut = QMutex()
        self.isexit = False

    def run(self):
        while (True):
            self.qmut.lock()
            if (self.isexit):
                break
            self.qmut.unlock()
            self._signal_serial.emit()
            # 绘图频率
            time.sleep(0.1)
        self.qmut.unlock()

    def stop(self):
        # 改变线程状态与终止
        self.qmut.lock()
        self.isexit = True
        self.qmut.unlock()
        self.wait()