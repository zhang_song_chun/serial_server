# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created by: PyQt5 UI code generator 5.15.1
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.

import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QWidget


class Ui_login_Form(object):
    def setupUi(self, login_Form):
        login_Form.setObjectName("login_Form")
        login_Form.resize(1000, 650)
        self.label = QtWidgets.QLabel(login_Form)
        self.label.setGeometry(QtCore.QRect(150, 80, 711, 421))
        self.label.setText("")
        self.label.setPixmap(QPixmap('pic/login.jpg'))
        self.label.setObjectName("label")
        self.login_pushButton = QtWidgets.QPushButton(login_Form)
        self.login_pushButton.setGeometry(QtCore.QRect(680, 520, 141, 41))
        self.login_pushButton.setObjectName("login_pushButton")

        self.retranslateUi(login_Form)
        QtCore.QMetaObject.connectSlotsByName(login_Form)

    def retranslateUi(self, login_Form):
        _translate = QtCore.QCoreApplication.translate
        login_Form.setWindowTitle(_translate("login_Form", "登录"))
        self.login_pushButton.setText(_translate("login_Form", "登录"))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = QWidget()
    ui = Ui_login_Form()
    ui.setupUi(form)
    form.show()
    sys.exit(app.exec_())