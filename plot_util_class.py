from PyQt5.QtChart import QChart, QSplineSeries, QValueAxis
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPen
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

class Spline(QChart):
    def __init__(self):
        super().__init__()
        self.resize(300,300)
        self.m_x = 0
        self.m_y = 0
        self.min, self.max = 0, 10
        self.sampleRate = 1
        self.xRange = 500
        self.count = 1
        self.ydata_min, self.ydata_max = 5, 10
        self.series = QSplineSeries(self)
        # 初始化图像
        red_pen = QPen(Qt.red)
        red_pen.setWidth(2)
        self.series.setPen(red_pen)
        self.series.setUseOpenGL(True)
        self.axisX = QValueAxis()
        self.axisY = QValueAxis()

        self.series.append(self.m_x, self.m_y)
        self.addSeries(self.series)
        self.addAxis(self.axisX, Qt.AlignBottom)
        self.addAxis(self.axisY, Qt.AlignLeft)
        self.series.attachAxis(self.axisX)
        self.series.attachAxis(self.axisY)
        self.series.setUseOpenGL(True)
        self.axisX.setTickCount(5)
        self.axisX.setRange(0, self.xRange)
        self.axisY.setRange(-6, 15)

    def handleUpdate(self, ydata):
        if (self.count < self.xRange):
            for i in range(self.sampleRate):
                if ydata < self.min: self.min = ydata
                if ydata > self.max: self.max = ydata

                self.series.append(self.count + i, ydata)
                self.axisY.setRange(self.min - 0.1 * (self.max - self.min), \
                                    self.max + 0.1 * (self.max - self.min))
        else:
            points = self.series.pointsVector()
            y_temp = [0] * (len(points) - self.sampleRate)
            for i in range(len(points) - self.sampleRate):
                points[i].setY(points[i + self.sampleRate].y())
                y_temp[i] = points[i + self.sampleRate].y()
            for i in range(self.sampleRate):
                points[len(points) - (self.sampleRate - i)].setY(ydata)
            self.series.replace(points)
            self.axisY.setRange(min(y_temp) - 0.1 * (max(y_temp) - min(y_temp)),\
                                max(y_temp) + 0.1 * (max(y_temp) - min(y_temp)))
            # self.axisX.setRange(self.count - self.xRange, self.count)
        self.count += self.sampleRate

class MyplotFigure(FigureCanvas):
    def __init__(self, parent = None, width = 10, height = 10, dpi = 100):
        # plt.rcParams['figure.facecolor'] = 'r'  # 设置窗体颜色
        # plt.rcParams['axes.facecolor'] = 'b' # 设置绘图区颜色
        self.figs = Figure(figsize=(width, height), dpi = dpi)
        super(MyplotFigure, self).__init__(self.figs)
        self.axes = self.figs.add_subplot(111)

        self.setParent(parent)
        FigureCanvas.updateGeometry(self)

    def matplot_drow_axes(self, x, y):
        self.axes.cla()
        self.axes.grid()
        self.axes.plot(x, y)
        # self.axes.set_ylabel(fontsize = 17)
        self.figs.canvas.draw()  # 这里注意是画布重绘，self.figs.canvas
        self.figs.canvas.flush_events()  # 画布刷新self.figs.canvas
